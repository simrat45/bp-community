<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';

    protected $guarded = [];

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers','user_id','follower_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id');
    }
}
