<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostData extends Model
{
    protected $table = 'post_data';
    protected $guarded = [];

    public function likedUser()
    {
        return $this->belongsTo(User::class, 'liked_user_id');
    }

    public function commentUser()
    {
        return $this->belongsTo(User::class, 'comment_user_id');
    }
}
