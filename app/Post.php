<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function postUser()
    {
        return $this->belongsTo(User::class, 'post_user_id');
    }

    public function likes()
    {
        return $this->hasMany(PostData::class, 'post_id')->whereNull('comment_user_id');
    }

    public function comments()
    {
        return $this->hasMany(PostData::class, 'post_id')->whereNull('liked_user_id');
    }
}
