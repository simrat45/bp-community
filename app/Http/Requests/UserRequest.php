<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function wantJson()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => '',
            'name' => '',
            'groups' => 'array',
            'business_name' => '',
            'profile_pic' => '',
            'address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'latitude' => '',
            'longitude' => '',
        ];
    }
}
