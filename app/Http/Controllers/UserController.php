<?php

namespace App\Http\Controllers;

use App\Follower;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function getDetails($me)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        return UserResource::make($user);
    }

    public function all()
    {
        $user = User::get();
        return UserResource::collection($user);
    }

    public function postDetails($me, UserRequest $request)
    {
        $user = User::firstOrNew(['username' => $me]);
        $data = $request->all();
        if ($request->has('groups')) {
            $data['groups'] = implode(",", $request->get('groups'));
        }
        if ($request->has('profile_pic')) {
            $imgdata = explode("base64,", str_replace(" ", "+", $request->profile_pic));
            $img = $imgdata[1];
            $mime_type = preg_replace("/[^a-zA-z\/]/", "", explode(':', $imgdata[0])[1]);
            $extension = explode("/", $mime_type);
            $decoded_image = base64_decode($img);
            if (isset($imgdata[1]) && strlen($imgdata[1]) > 0) {
                $fileName = "profile_" . $me . "_" . now()->timestamp . "." . $extension[1];
                $upload_status = Storage::put('public/' . $fileName, $decoded_image, ["ContentEncoding" => 'base64', "ContentType" => $mime_type]);
            }
            if (!empty($upload_status))
                $data['profile_pic'] = $fileName;
            else
                unset($data['profile_pic']);
        }
        $user->fill($data)->save();
        return $this->getDetails($user->username);
    }

    public function followers($me)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $followers = $user->followers;
        return UserResource::collection($followers);
    }


    public function follow($me, $username, $unfollow = null)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $followUser = User::where('username', $username)->first();
        if (empty($followUser)) {
            return response()->json(["message" => 'This user you\'re trying to follow doesn\'t exist!'], 422);
        }
        $follower = Follower::firstOrNew(['user_id' => $followUser->id, 'follower_id' => $user->id]);
        if ($unfollow == 'unfollow') {
            $follower->delete();
        } else {
            $follower->save();
        }
        return $this->followers($user->username);
    }

}
