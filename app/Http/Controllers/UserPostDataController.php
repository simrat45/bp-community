<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use App\Post;
use App\PostData;
use App\User;
use Illuminate\Http\Request;


class UserPostDataController extends Controller
{
    public function postLike($me, $postUuId, $likedUserName, $unlike = null)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $post = Post::where('uuid', $postUuId)->first();
        if (empty($post)) {
            return response()->json(["message" => 'This post doesn\'t exist!'], 404);
        }
        $likedUser = User::where('username', $likedUserName)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user you\'ve liked doesn\'t exist!'], 404);
        }
        $postData = PostData::firstOrNew(['post_id' => $post->id, 'liked_user_id' => $user->id]);
        if ($unlike == 'unlike') {
            $postData->delete();
        } else {
            $postData->uuid = uniqid();
            $postData->save();
        }
        $post->load('user','postUser','likes.likedUser','comments.commentUser');
        return PostResource::make($post);
    }

    public function postComment($me, $postUuId, $commentUserName, Request $request, $commentUuid = null)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $post = Post::where('uuid', $postUuId)->first();
        if (empty($post)) {
            return response()->json(["message" => 'This post doesn\'t exist!'], 404);
        }
        $commentUser = User::where('username', $commentUserName)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user you\'ve liked doesn\'t exist!'], 404);
        }
        if ($commentUuid) {
            $postData = PostData::where(['post_id' => $post->id, 'comment_user_id' => $user->id, 'uuid' => $commentUuid]);
            $postData->delete();
        } else {
            $postData = new PostData();
            $postData->uuid = uniqid();
            $postData->post_id = $post->id;
            $postData->comment_user_id = $user->id;
            $postData->comment = $request->comment ?? null;
            $postData->save();
        }
        $post->load('user','postUser','likes.likedUser','comments.commentUser');
        return PostResource::make($post);
    }
}
