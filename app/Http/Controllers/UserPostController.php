<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserPostController extends Controller
{
    public function getFeed($me, Request $request)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $groups = explode(',', $user->groups);
        $followerIds = $user->followers()->get()->pluck('id')->all();
        $userIds = array_merge($followerIds, [$user->id]);
        $posts = Post::with(['user', 'postUser','likes.likedUser','comments.commentUser'])->whereIn('user_id', $userIds)->orWhereIn('group', $groups)->get();
        return PostResource::collection($posts);
    }

    public function createPost($me, PostRequest $request)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $post = new Post();
        $data = $request->all();
        if ($request->has('post_username')) {
            $postUser = User::where('username', $request->post_username)->first();
            if (empty($postUser)) {
                return response()->json(["message" => 'This user you\'re posting on doesn\'t exist!'], 422);
            }
            $data['post_user_id'] = $postUser->id;
            unset($data['post_username']);
        }
        if ($request->has('attachment') && !empty($request->attachment)) {
            $imgdata = explode("base64,", str_replace(" ", "+", $request->attachment));
            $img = $imgdata[1];
            $mime_type = preg_replace("/[^a-zA-z\/]/", "", explode(':', $imgdata[0])[1]);
            $extension = explode("/", $mime_type);
            $decoded_image = base64_decode($img);
            if (isset($imgdata[1]) && strlen($imgdata[1]) > 0) {
                $fileName = "attachment_" . $me . "_" . now()->timestamp . "." . $extension[1];
                $upload_status = Storage::put('public/' . $fileName, $decoded_image, ["ContentEncoding" => 'base64', "ContentType" => $mime_type]);
            }
            if (!empty($upload_status))
                $data['attachment'] = $fileName;
            else
                unset($data['attachment']);
        }
        $data['uuid'] = uniqid();
        $data['like_count'] = 0;
        $data['comment_count'] = 0;

        $data['uuid'] = uniqid();
        $data['user_id'] = $user->id;
        $post->fill($data)->save();
        return response()->json(["message" => 'The Post has been uploaded'], 200);
    }

    public function getGroupPosts($me, $groupName = null)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $groups = explode(',', $user->groups);
        if ($groupName) {
            $groups = [$groupName];
        }
        $posts = Post::with(['user', 'postUser','likes.likedUser','comments.commentUser'])->whereIn('group', $groups)->get();
        return PostResource::collection($posts);
    }

    public function getUserPosts($me, $postUserName = null)
    {
        $user = User::where('username', $me)->first();
        if (empty($user)) {
            return response()->json(["message" => 'This user doesn\'t exist!'], 404);
        }
        $posts = Post::with(['user', 'postUser','likes.likedUser','comments.commentUser'])->where('user_id', $user->id);
        if ($postUserName) {
            $postUser = User::where('username', $postUserName)->first();
            if (empty($postUser)) {
                return response()->json(["message" => 'This user you have requested doesn\'t exist!'], 404);
            }
            $posts = $posts->where('post_user_id', $postUser->id);
        }
        $posts = $posts->get();
        return PostResource::collection($posts);
    }
}
