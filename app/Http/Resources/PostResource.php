<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'group' => $this->group,
            'like_count' => !(empty($this->likes) || $this->likes->isEmpty()) ? $this->likes->count() : 0,
            'comment_count' => !(empty($this->comments) || $this->comments->isEmpty()) ? $this->comments->count() : 0,
            'content' => $this->content,
            'attachment' => empty($this->attachment) ? null : url(Storage::url($this->attachment)),
            'user' => UserResource::make($this->whenLoaded("user")),
            'post_user' => UserResource::make($this->whenLoaded('postUser')),
            'likes' => LikeResource::collection($this->whenLoaded('likes')),
            'comments' => CommentResource::collection($this->whenLoaded('comments')),
            'created_at' => $this->created_at,
        ];
    }
}
