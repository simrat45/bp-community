<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "username" => $this->username,
            "name" => $this->name,
            "mobile" => $this->mobile,
            "groups" => empty($this->groups) ? [] : explode(',', $this->groups),
            "profile_pic" => empty($this->profile_pic) ? null : url(Storage::url($this->profile_pic)),
            "business_name" => $this->business_name,
            "address" => $this->address,
            "city" => $this->city,
            "state" => $this->state,
            "pincode" => $this->pincode,
            "latitude" => $this->latitude,
            "longitude" => $this->longitude,
            "status" => $this->status
        ];
    }
}
