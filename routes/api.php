<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function () {
    Route::get('/', 'UserController@all');
    Route::get('/{me}', 'UserController@getDetails');

    Route::post('/{me}', 'UserController@postDetails');
    Route::post('{me}/follow/{username}/{unfollow?}', 'UserController@follow');

    Route::get('{me}/followers', 'UserController@followers');
    Route::get('{me}/posts/feed', 'UserPostController@getFeed');
    Route::get('{me}/posts/group/{group?}', 'UserPostController@getGroupPosts');
    Route::get('{me}/posts/user/{user?}', 'UserPostController@getUserPosts');

    Route::post('{me}/posts', 'UserPostController@createPost');
    Route::post('{me}/posts/{uuid}/like/{username}/{unlike?}', 'UserPostDataController@postLike');
    Route::post('{me}/posts/{uuid}/comment/{username}/{commentUuid?}', 'UserPostDataController@postComment');

});
